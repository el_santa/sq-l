import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()

export class GoogleBooksService {

	private bookUrl: string = 'https://www.googleapis.com/books/v1/volumes';

	constructor(private http: HttpClient) { }

	getBooks(bookName: string, startIndex: number, maxResult:number) {
		return new Observable((observer) => {
			this.http.get(`${this.bookUrl}?q=${encodeURIComponent(bookName)}&maxResults=${maxResult}&startIndex=${startIndex}`).subscribe((responce: any) => {
				if (responce && responce.items && responce.items.length) observer.next(responce);
				else observer.error({message: 'no results'});
			}, (err) => {
				observer.error((err.error && err.error.error) || err);
			});
		})
	}
}