import { Component, OnInit, NgModule } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
	selector: 'app-login-screen',
	templateUrl: './login-screen.component.html',
	styleUrls: ['./login-screen.component.less']
})

export class LoginScreenComponent implements OnInit {
	title:string = "Hello guest. Enter your credentials";

	myGroup = new FormGroup({
		name: new FormControl('', Validators.required),
		password: new FormControl('', [Validators.pattern('123456'), Validators.required])
	});

	constructor(private dialogRef: MatDialogRef<LoginScreenComponent>) { }

	ngOnInit() {

	}

	submit() {
		if (this.myGroup.valid) {
			this.dialogRef.close(this.myGroup.controls.name.value);
		}
	}
}
