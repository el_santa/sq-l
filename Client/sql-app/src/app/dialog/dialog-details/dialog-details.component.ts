import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-details',
  templateUrl: './dialog-details.component.html',
  styleUrls: ['./dialog-details.component.less']
})
export class DialogDetailsComponent{

	private title:string;
	private description: string;
	private image:string;
	private thumnailLoading: boolean = true;
	private noDescription: string = 'no description';

  constructor( public dialogRef: MatDialogRef<DialogDetailsComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {

		this.title = data.title;
		this.description = data.description;
		this.image = data.image;
   }

}