import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
	selector: 'app-dialog-error',
	templateUrl: './dialog-error.component.html',
	styleUrls: ['./dialog-error.component.less']
  })
export class DialogErrorComponent{

	private errorMsg:string;

  constructor( public dialogRef: MatDialogRef<DialogErrorComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
		this.errorMsg = data;
   }
}
