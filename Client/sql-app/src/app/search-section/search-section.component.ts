import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material";
import { LoginScreenComponent } from '../login-screen/login-screen.component';
import { GoogleBooksService } from '../services/google-books.service';
import { DialogDetailsComponent } from '../dialog/dialog-details/dialog-details.component';
import { DialogErrorComponent } from '../dialog/dialog-error/dialog-error.component';

@Component({
	selector: 'app-search-section',
	templateUrl: './search-section.component.html',
	styleUrls: ['./search-section.component.less'],
	providers: [GoogleBooksService]
})
export class SearchSectionComponent implements OnInit {

	private title:string= 'SQLINK-app';
	private bookName:string = '';
	private userName:string = '';
	private bookList:any = [];
	private loading:boolean = false;
	private pageStartIndex: number = 1;
	private paginator:any = {
		totalCount: 0,
		size: 20
	};

	constructor(private dialog: MatDialog, private googleBooksService: GoogleBooksService) {}

	openLoginDialog() {
		const dialogConfig = new MatDialogConfig();

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;

		this.dialog.open(LoginScreenComponent, dialogConfig).afterClosed().subscribe(result => {
			this.userName = result.charAt(0).toUpperCase() + result.slice(1);
		});
	}

	ngOnInit() {
		this.openLoginDialog();
	}

	searchBooks(startIndex:number = 0){
		if(this.bookName == '') return;

		this.loading = true;
		this.googleBooksService.getBooks(this.bookName, startIndex, this.paginator.size).subscribe((result:any) => {
			this.bookList = [];
			this.paginator.totalCount = result.totalItems;
			this.pageStartIndex = startIndex + 1;

			result.items.forEach(book => {
				this.bookList.push({title: book.volumeInfo.title, description: book.volumeInfo.description, image: book.volumeInfo.imageLinks ? book.volumeInfo.imageLinks.thumbnail : ''});
				this.loading = false;
			});
		}, (err => {
			this.dialog.open(DialogErrorComponent, {data: err.message});
			this.loading = false;
		}));
	}

	showDetails(title, description, image){
		const dialogConfig = new MatDialogConfig();

		dialogConfig.autoFocus = true;
		dialogConfig.data = {
			title: title,
			description: description,
			image: image
		};
		
		this.dialog.open(DialogDetailsComponent, dialogConfig);
	}

	getPage(paginatorData){
		this.searchBooks(paginatorData.pageIndex * paginatorData.pageSize);
	}
}
