import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchSectionComponent } from './search-section/search-section.component';
import { LoginScreenComponent } from './login-screen/login-screen.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatPaginatorModule } from '@angular/material/paginator'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDialogModule} from "@angular/material";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from  '@angular/common/http';
import { DialogDetailsComponent } from './dialog/dialog-details/dialog-details.component';
import { DialogErrorComponent } from './dialog/dialog-error/dialog-error.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchSectionComponent,
    LoginScreenComponent,
    DialogDetailsComponent,
    DialogErrorComponent
  ],
  imports: [
    BrowserModule,
	AppRoutingModule,
	MatFormFieldModule,
	MatInputModule,
	BrowserAnimationsModule,
	MatDialogModule,
	FormsModule,
	ReactiveFormsModule,
	MatButtonModule,
	MatToolbarModule,
	MatCardModule,
	MatDividerModule,
	MatListModule,
	HttpClientModule,
	MatProgressBarModule,
	MatPaginatorModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [LoginScreenComponent, DialogDetailsComponent, DialogErrorComponent]
})
export class AppModule { }
