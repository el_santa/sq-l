﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Server.Data;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Middleware
{
    public class ReportMiddleware
    {
        private readonly RequestDelegate _next;

        public ReportMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            using (MemoryStream requestBodyStream = new MemoryStream())
            {
                using (MemoryStream responseBodyStream = new MemoryStream())
                {
                    Stream originalRequestBody = context.Request.Body;
                    context.Request.EnableRewind();
                    Stream originalResponseBody = context.Response.Body;

                    await context.Request.Body.CopyToAsync(requestBodyStream);
                    requestBodyStream.Seek(0, SeekOrigin.Begin);

                    string requestBody = new StreamReader(requestBodyStream).ReadToEnd();

                    requestBodyStream.Seek(0, SeekOrigin.Begin);
                    context.Request.Body = requestBodyStream;

                    string responseBody = "";


                    context.Response.Body = responseBodyStream;

                    Stopwatch watch = Stopwatch.StartNew();
                    await _next(context);
                    watch.Stop();

                    responseBodyStream.Seek(0, SeekOrigin.Begin);
                    responseBody = new StreamReader(responseBodyStream).ReadToEnd();

                    ReportDataHandler.AddReport(new ReportModel
                    {
                        Host = context.Request.Host.Host,
                        Path = context.Request.Path,
                        Query = context.Request.QueryString.ToString(),
                        IP = context.Connection.RemoteIpAddress.MapToIPv4().ToString(),
                        Headers = string.Join(",", context.Request.Headers.Select(h => h.Key + ":[" + h.Value + "]").ToList()),
                        RequestBody = requestBody,
                        Date = DateTime.Now,
                        ExecutionTime = watch.ElapsedMilliseconds,
                        Status = context.Response.StatusCode,
                        Method = context.Request.Method
                    });

                    responseBodyStream.Seek(0, SeekOrigin.Begin);

                    await responseBodyStream.CopyToAsync(originalResponseBody);

                    context.Request.Body = originalRequestBody;
                    context.Response.Body = originalResponseBody;
                }
            }
        }
    }
}
