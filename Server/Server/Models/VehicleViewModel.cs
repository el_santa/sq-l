﻿using System;
using System.Runtime.Serialization;

namespace Server.Models
{
    public class VehicleViewModel
    {
        public string Model { get; set; }
        public string Owner { get; set; }
        public int Year { get; set; }
        public string LicenseNumber { get; set; }
        public int Power { get; set; }
        public int TopSpeed { get; set; }
        public float Acceleration { get; set; }
        public float Toraue { get; set; }
        public float CO2Emmision { get; set; }
        public short EuroEmissionsStandart { get; set; }
        public int MilesPerTank { get; set; }
        public int Height { get; set; }
        public int WheelBase { get; set; }
        public float TurningCircle { get; set; }
        public float EngineSize { get; set; }
        public short Cylinders { get; set; }
        public short Valves { get; set; }
        public string FuelType { get; set; }
        public string Transmission { get; set; }
        public string GearBox { get; set; }
        public string DriveTrain { get; set; }
        public int Length { get; set; }

        private string Stringify()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        public override bool Equals(object obj)
        {
            return Stringify() == (obj as VehicleViewModel)?.Stringify();
        }
    }
}
