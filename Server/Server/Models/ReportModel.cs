﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Runtime.Serialization;

namespace Server.Models
{
    public class ReportModel
    {
        public string Host { get; set; }
        public string Path { get; set; }
        public string Query { get; set; }
        public string IP { get; set; }
        [IgnoreDataMember]
        public string Headers { get; set; }
        public string RequestBody { get; set; }
        public DateTime Date { get; set; }
        public double ExecutionTime { get; set; }
        public int Status { get; set; }
        public string Method { get; set; }
        public IActionResult Result{ get; set; }
    }
}
