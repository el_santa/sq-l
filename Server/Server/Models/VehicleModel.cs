﻿using Server.Storage;
using System;

namespace Server.Models
{
    public class VehicleModel
    {
        public long ID { get; set; }
        public VehicleViewModel ViewModel;
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public VehicleModel(VehicleViewModel viewModel)
        {
            ViewModel = viewModel;
            UpdatedDate = CreatedDate = DateTime.Now;
            ID = IDStorage.CurrentID;
        }

        public void Update(VehicleViewModel updateModel)
        {
            ViewModel = updateModel;
            UpdatedDate = DateTime.Now;
        }
    }
}
