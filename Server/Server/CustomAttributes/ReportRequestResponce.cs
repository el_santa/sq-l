﻿using Microsoft.AspNetCore.Mvc.Filters;
using Server.Data;
using Server.Models;
using System;
using System.IO;
using System.Linq;

namespace Server.CustomAttributes
{
    public class ReportRequestResponce : ActionFilterAttribute
    {
        const string CONTEXT_START_TIME_NAME = "StartTime";
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.HttpContext.Items[CONTEXT_START_TIME_NAME] = DateTime.UtcNow;
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            DateTime startTime = (DateTime)filterContext.HttpContext.Items[CONTEXT_START_TIME_NAME];
            double executionTine = (DateTime.UtcNow - startTime).TotalMilliseconds;

            ReportModel currentReport = new ReportModel
            {
                Host = filterContext.HttpContext.Request.Host.Host,
                Path = filterContext.HttpContext.Request.Path,
                Query = filterContext.HttpContext.Request.QueryString.ToString(),
                IP = filterContext.HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString(),
                Headers = string.Join(",", filterContext.HttpContext.Request.Headers.Select(h => h.Key + ":[" + h.Value + "]").ToList()),
                Date = startTime,
                ExecutionTime = executionTine,
                Status = filterContext.HttpContext.Response.StatusCode,
                Method = filterContext.HttpContext.Request.Method,
                Result = filterContext.Result
            };

            if (filterContext.HttpContext.Request.Body.CanSeek)
            {
                filterContext.HttpContext.Request.Body.Seek(0, SeekOrigin.Begin);

                using (var reader = new StreamReader(filterContext.HttpContext.Request.Body))
                {
                    currentReport.RequestBody = reader.ReadToEnd();
                }
            }

            ReportDataHandler.AddReport(currentReport);

            base.OnActionExecuted(filterContext);
        }
    }
}
