﻿using Server.Models;
using Server.Storage;
using Server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.Data
{
    public static class VehicleDataHandler
    {
        public static List<VehicleModel> GetAll()
        {
            return VehicleStorage.VehicleList;
        }
        public static List<VehicleModel> GetListFiltered(string name, string value)
        {
            return VehicleStorage.VehicleList
                .Where(
                    m => m.ViewModel.GetType().GetProperty(SharedFunctions.Capitalize(name))
                    .GetValue(m.ViewModel).ToString().ToLower() == value.ToLower()
                )
                .ToList();
        }

        public static void Create(VehicleViewModel newVehicle)
        {
            if (VehicleStorage.VehicleList.Where(m => m.ViewModel.Equals(newVehicle)).Count() != 0)
            {
                throw new ArgumentException();
            }
            else
            {
                VehicleStorage.VehicleList.Add(new VehicleModel(newVehicle));
            }
        }

        public static void Update(long[] ids, VehicleViewModel newVehicle)
        {
            for (int i = 0; i < VehicleStorage.VehicleList.Count(); i++)
            {
                if (ids.Contains(VehicleStorage.VehicleList[i].ID))
                    VehicleStorage.VehicleList[i].Update(newVehicle);
            }
        }

        public static void Delete(long[] ids)
        {
            int count = VehicleStorage.VehicleList.Count() - 1;

            for (int i = count; i >= 0; i--)
            {
                if (ids.Contains(VehicleStorage.VehicleList[i].ID))
                    VehicleStorage.VehicleList.RemoveAt(i);
            }
        }

    }
}
