﻿using Microsoft.AspNetCore.Mvc;
using Server.Enums;
using Server.Models;
using Server.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Server.Data
{
    public static class ReportDataHandler
    {
        public static void AddReport(ReportModel newReport)
        {
            ReportStorage.ReportList.Add(newReport);
        }

        public static List<ReportModel> GetReports(string typeString)
        {
            Enum.TryParse(typeString, out ReportTypes type);
            TimeSpan timeSpan;

            switch (type)
            {
                case ReportTypes.Hour:
                    timeSpan = new TimeSpan(1, 0, 0);
                    break;
                case ReportTypes.Day:
                    timeSpan = new TimeSpan(1, 0, 0, 0);
                    break;
                case ReportTypes.Week:
                    timeSpan = new TimeSpan(7, 0, 0, 0);
                    break;
                case ReportTypes.Month:
                    timeSpan = new TimeSpan(30, 0, 0, 0);
                    break;
                case ReportTypes.Year:
                    timeSpan = new TimeSpan(365, 0, 0, 0);
                    break;
                default:
                    throw new ArgumentException();
            }

            return ReportStorage.ReportList.Where(r => r.Date.Add(timeSpan) >= DateTime.Now).ToList();
        }

        public static List<long> GetIDsSortedByMostUsed()
        { 
            List<Tuple<long?, int>> sortedIdsFromQuery = ReportStorage.ReportList
                .GroupBy(list => list.Query, (key, grp) => new { ID = Regex.Match(key, @"ids=(\d+)").Groups[1].Value, Count = grp.Count() })
                .Where(resObj => !string.IsNullOrEmpty(resObj.ID))
                .OrderByDescending(resObj => resObj.Count)
                .Select(resObj => new Tuple<long?,int> (long.Parse(resObj.ID), resObj.Count ))
                .ToList();

            List<Tuple<long?, int>> sortedIdsFromResult = ReportStorage.ReportList
                .Select(list => list.Result as ObjectResult)
                .Where(list => list != null)
                .Select(list => list)
                .SelectMany(list => list.Value as List<VehicleModel>)
                .GroupBy(IRep => IRep.ID, (key, grp) => new { ID = key, Count = grp.Count() })
                .Where(resObj => resObj != null)
                .OrderByDescending(resObj => resObj.Count)
                .Select(resObj => new Tuple<long?, int>(resObj.ID, resObj.Count))
                .ToList();


            return sortedIdsFromQuery.Concat(sortedIdsFromResult).GroupBy(tupl => tupl.Item1, (key, grp) => new { ID = key ?? -1, Count = grp.Count() })
                .Where(res=>res.ID != -1)
                 .OrderByDescending(res => res.Count)
                 .Select( res=> res.ID)
                 .ToList();
        }
    }
}