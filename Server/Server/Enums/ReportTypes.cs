﻿namespace Server.Enums
{
    public enum ReportTypes
    {
        Hour,
        Day,
        Week,
        Month,
        Year
    }
}
