﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Utils
{
    public static class SharedFunctions
    {
        /// <summary>
        /// Convert  <paramref name="inputStr"/> to lower case with first letter upper cased
        /// </summary>
        /// <param name="inputStr"></param>
        /// <returns></returns>
        public static string Capitalize(string inputStr)
        {
            if (string.IsNullOrWhiteSpace(inputStr))
                return inputStr;
            else
                return inputStr.First().ToString().ToUpper() + inputStr.Substring(1).ToLower();
        }
    }
}
