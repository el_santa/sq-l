﻿using System;
namespace Server.Storage
{
    public static class IDStorage
    {
        private static long currentId;
        public static long  CurrentID {
            get { return currentId++; }
        }
    }
}
