﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.CustomAttributes;
using Server.Data;
using Server.Models;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReportRequestResponce]
    public class VehicleController : ControllerBase
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<VehicleModel>> GetList([FromQuery] string propertyName, [FromQuery] string propertyValue)
        {
            try
            {
                HttpContext.Response.StatusCode = StatusCodes.Status200OK;

                if (string.IsNullOrWhiteSpace(propertyName))
                    return VehicleDataHandler.GetAll();
                else
                    return VehicleDataHandler.GetListFiltered(propertyName, propertyValue);

            }
            catch
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public void Create([FromBody] VehicleViewModel newVehicle)
        {
            try
            {
                VehicleDataHandler.Create(newVehicle);
                HttpContext.Response.StatusCode = StatusCodes.Status201Created;
            }
            catch (ArgumentException)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status409Conflict;
            }
            catch
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public void Update([FromQuery] long [] ids, [FromBody]VehicleViewModel newVehicle)
        {
            try
            {
                VehicleDataHandler.Update(ids, newVehicle);
                HttpContext.Response.StatusCode = StatusCodes.Status204NoContent;
            }
            catch
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public void Delete([FromQuery] long [] ids)
        {
            try
            {
                VehicleDataHandler.Delete(ids);
                HttpContext.Response.StatusCode = StatusCodes.Status204NoContent;
            }
            catch
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }
        }
    }
}
