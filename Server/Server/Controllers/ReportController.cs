﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Data;
using Server.Models;
using Server.Utils;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<ReportModel>> GetReports([FromQuery] string reportType)
        {
            try
            {
                HttpContext.Response.StatusCode = StatusCodes.Status200OK;

                return ReportDataHandler.GetReports(SharedFunctions.Capitalize(reportType));
            }
            catch
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }

        [HttpGet("mostUsedIds")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<long>> GetIDsSortedByMostUsed()
        {
            try
            {
                HttpContext.Response.StatusCode = StatusCodes.Status200OK;

                return ReportDataHandler.GetIDsSortedByMostUsed();
            }
            catch
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }
    }
}